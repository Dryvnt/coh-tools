#!/usr/bin/env python3

import argparse
import xml.etree.ElementTree as ET
import sys
import yaml

parser = argparse.ArgumentParser(
    description='Simple visual heightmap generator for Cadence of Hyrule')
parser.add_argument('mapfile', type=argparse.FileType('r'),
                    help='File to read tile data from')
parser.add_argument('--no-fence', action='store_true',
                    help='Do not print fences')
parser.add_argument('--no-stairs', action='store_true',
                    help='Do not print stairs')
parser.add_argument('--no-water', action='store_true',
                    help='Do not use pretty water characters (also lava/posion)')
parser.add_argument('--no-tree', action='store_true',
                    help='Do not print trees')
parser.add_argument('--no-crate', action='store_true',
                    help='Do not print crates')
parser.add_argument('--no-chest', action='store_true',
                    help='Do not print chests')

args = parser.parse_args()

map_string = args.mapfile.read()
map_data = ET.fromstring(map_string)

error_char = None
tile_chars = None
fence_chars = None
tree_chars = None
crate_chars = None
chest_chars = None
with open('char_data.yaml', 'r') as f:
    char_data = yaml.safe_load(f)

    error_char = char_data['general_error_char']
    tile_chars = char_data['tile_chars']
    fence_chars = char_data['fence_chars']
    tree_chars = char_data['tree_chars']
    crate_chars = char_data['crate_chars']
    chest_chars = char_data['chest_chars']

errors = []

treemap = {}
trees = map_data.find('trees')
for tree in trees:
    attribs = tree.attrib
    x = int(attribs['x'])
    y = int(attribs['y'])
    t = int(attribs['type'])

    if t in tree_chars['four_tile_trees']:
        treemap[(x, y)] = t
        treemap[(x + 1, y)] = t
        treemap[(x, y + 1)] = t
        treemap[(x + 1, y + 1)] = t
    else:
        treemap[(x, y)] = t

cratemap = {}
crates = map_data.find('crates')
for crate in crates:
    attribs = crate.attrib
    x = int(attribs['x'])
    y = int(attribs['y'])
    t = int(attribs['type'])

    if t in crate_chars['three_tile_wide_crates']:
        cratemap[(x, y)] = t
        cratemap[(x + 1, y)] = t
        cratemap[(x + 2, y)] = t
    else:
        cratemap[(x, y)] = t

chestmap = {}
chests = map_data.find('chests')
for chest in chests:
    attribs = chest.attrib
    x = int(attribs['x'])
    y = int(attribs['y'])
    contents = attribs['contents']
    chestmap[(x, y)] = contents


heightmap = {}
tiles = map_data.find('tiles')
for tile in tiles:
    attribs = tile.attrib
    x = int(attribs['x'])
    y = int(attribs['y'])
    height = int(attribs['height'])
    t = int(attribs['type'])
    f = {'U': False, 'D': False, 'L': False, 'R': False}
    for fence in tile.find('fences'):
        f_attribs = fence.attrib
        if f_attribs['direction'] == '0':
            f['R'] = True
        if f_attribs['direction'] == '1':
            f['D'] = True
        if f_attribs['direction'] == '2':
            f['L'] = True
        if f_attribs['direction'] == '3':
            f['U'] = True

    if (x, y) in heightmap:
        errors.append(f'Overwriting tile {x},{y}')
    heightmap[(x, y)] = {'type': t, 'height': height, 'fence': f}

cell_width = 1
max_y = 0
max_x = 0
for ((x, y), data) in heightmap.items():
    height = data['height']
    cell_width = max(cell_width, len(str(height)))
    max_y = max(max_y, y)
    max_x = max(max_x, x)

assert(cell_width == 1) # if this doesn't hold, things will be uggo

def has_fence(x, y):
    f = heightmap[(x, y)]['fence']
    if args.no_fence:
        return False
    nf = {'U': False, 'D': False, 'L': False, 'R': False}
    return f != nf

def has_tree(x, y):
    if args.no_tree:
        return False
    return (x, y) in treemap


def has_crate(x, y):
    if args.no_crate:
        return False
    return (x, y) in cratemap


def has_chest(x, y):
    if args.no_chest:
        return False
    return (x, y) in chestmap

def print_fence(x, y):
    f_dict = heightmap[(x, y)]['fence']
    f = ''
    if f_dict['U']:
        f += 'U'
    if f_dict['D']:
        f += 'D'
    if f_dict['L']:
        f += 'L'
    if f_dict['R']:
        f += 'R'
    if f in fence_chars:
        print(fence_chars[f], end='')
    else:
        errors.append(f'No fence character for {f} at {x},{y}')
        print(error_char, end='')

def print_tree(x, y):
    t = treemap[(x, y)]
    if t in tree_chars:
        print(tree_chars[t], end='')
    else:
        errors.append(f'No tree character for {t} at {x},{y}')
        print(error_char, end='')

def print_crate(x, y):
    t = cratemap[(x, y)]
    if t in crate_chars:
        print(crate_chars[t], end='')
    else:
        errors.append(f'No crate character for {t} at {x},{y}')
        print(error_char, end='')


def print_chest(x, y):
    contents = chestmap[(x, y)]
    print(chest_chars['default'], end='')

def print_tile(x, y):
    tile = heightmap[(x, y)]
    t = tile['type']
    if t in tile_chars['stair_tiles'] and args.no_stairs:
        t = 0
    if t in tile_chars['water_tiles'] and args.no_water:
        t = 0
    if t in tile_chars:
        c = tile_chars[t]
        if c == '':
            c = tile['height']
        print(c, end='')
    else:
        errors.append(f'Tile type {t} at {x},{y} not known')
        print(tile_chars['missing_tile_char'], end='')



for y in range(max_y + 1):
    for x in range(max_x + 1):
        if (x, y) not in heightmap:
            print('?', end='')
            errors.append(f'{x},{y} has no tile data')
            continue
        f = heightmap[(x, y)]['fence']
        if has_tree(x, y):
            print_tree(x, y)
        elif has_crate(x, y):
            print_crate(x, y)
        elif has_chest(x, y):
            print_chest(x, y)
        elif has_fence(x, y):
            print_fence(x, y)
        else:
            print_tile(x, y)
    print('')

for error in errors:
    print(error, file=sys.stderr)
