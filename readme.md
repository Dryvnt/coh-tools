# Scripts

There is currently only one script, but I plan to extend this repository with any new ones I end up needing.

## heightmap.py

Provide a text-based top-down look of the map, to discover invisible walls and possible paths around fences.

# Getting the game data

The game data can be ripped from the system, and mostly consists of XML files. I will not help you rip the files, nor will I share any files with you. Getting the game data is your own responsibility.
